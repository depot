# INSTALL.md #

These are instruction on the requirements and how to install this software.

## Requirements ##

In order to install this software, you will need the following programs and 
libraries installed on your computer.
Note that for the dependencies referred to as optional, their availability
is optional, and only needed if you need the functionality given by those
depdencies.

  * Dependency X version > 3.X;
  * Dependency Y version >= 4.X.Z; (optional, if you required XXX (root beer))
  * tup (http://gittup.org/tup/);
  * tup-config (https://github.com/msteinert/tup-config);
  * pkg-config; (optional, if you are on Unix and want to detect the presence of
    some programs)


## Configuration ##

In order to configure this program, check that all requirements are present, and
any optional requirements you want, then in a console type:

  * tup-mconf (if you have ncurses (typically under an Unix or MacOSX)); OR
  * tup-nconf (if you have ncurses, and prefer this over tup-mconf); OR
  * tup-gconf (if you have gtk+ (typically under Gnome or Windows (with Gtk+))); OR
  * tup-qconf (if you have Qt (typically under KDE or Windows (with Qt)));

Then, follow the configuration screens and the instructions given in the sctipt
in order to configure the software the way you like.

## Building ##

After configuring the software, you can build it in the same shell using:

  * tup upd (you may also add -jN here were N is the number of update jobs 
    you want, normally somewhere between 1 to the number of processors your 
    machine has, but you must use this option carefully or you might fry your
    machine);

You will need to run this command in a shell that has the visual studio
variables if you are on windows and wish to compile using visual studio C++
compiler.

## Installing ##


