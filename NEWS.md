# NEWS #

This is a list of news for this project, listed from the oldest to the newest one.

## Release NN ( DD/MM/YYYY ) ##

These are the news for release NN:

### Bug and Security Fixes ###

These are a list of bug and security fixes in this release.

  * a;

### New Features ###

These are a list of new features in this release.

  * a;

### Refactorings ###

These are a list of refactorings present in this release.

  * a;
