The project authors, would like to thank to:

  * The GNU coreutils authors, for the ideas of how to 
    create the THANKS.md, AUTHORS.md and NEWS.md files;
  * The apache HTTPD authors, for the ideas of how to create 
    the INSTALL.md file;
  * The FSF for their excelent copyleft license;


