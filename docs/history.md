Historical Notes on depot
=========================

This document collects references to documentation, design documents,
code etc. that are no longer accurate nor used, but are of interest as
historical background for the current state of depot. A comprehensive
overview of the history of depot remains future work.

- [The TVL Monorepo](https://tvl.fyi/monorepo-doc),
  original (?) living design document for depot.
- [Sparse Checkouts](designs/SPARCE_CHECKOUTS.md),
  a proposal for a tool to create sparse checkouts of depot.

<!--
TODO(sterni): include other Google Docs I have links to?
Questionable whether it is worth it in all cases, as some of them may
have never been shared beyond ##?tvl(-dev)?. Interested in opinions.
-->
