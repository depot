{ depot, ... }:

(with depot.ops.machines; [
  sanduny
  bugry
  nevsky
]) ++

(with depot.users.tazjin.nixos; [
  camden
  frog
  tverskoy
  zamalek
]) ++

(with depot.users.aspen.system.system; [
  yeren
  mugwump
  ogopogo
  lusca
]) ++

(with depot.users.wpcarro.nixos; [
  ava
  kyoko
  marcus
  tarasco
])
