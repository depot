require 'helper'
require 'depot'

class TestDepot < Test::Unit::TestCase

  def test_version
    version = Depot.const_get('VERSION')

    assert !version.empty?, 'should have a VERSION constant'
  end

end
