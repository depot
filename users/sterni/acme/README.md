# //users/sterni/acme

Utilities and configurations related to the [acme][] text editor
from [Plan 9][].

## mkbqnkeyboard.bqn

Script to generate Plan 9 `/lib/keyboard` entries providing compose
sequences for the Unicode characters used by [BQN][]. For usage
instructions, refer to [its own documentation](./mkbqnkeyboard.md).

[acme]: https://9p.io/sys/doc/acme/acme.pdf
[Plan 9]: https://p9f.org/about.html
[BQN]: https://mlochbaum.github.io/BQN/
